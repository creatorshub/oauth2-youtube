<?php

namespace CreatorsHub\OAuth2\Client\Test\Provider;

use CreatorsHub\OAuth2\Client\Provider\YouTubeChannel;
use Eloquent\Phony\Phpunit\Phony;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use CreatorsHub\OAuth2\Client\Provider\YouTube as YouTubeProvider;
use League\OAuth2\Client\Token\AccessToken;

class GoogleTest extends \PHPUnit_Framework_TestCase
{
    protected $provider;

    protected function setUp()
    {
        $this->provider = new YouTubeProvider([
            'clientId' => 'mock_client_id',
            'clientSecret' => 'mock_secret',
            'redirectUri' => 'none',
            'hostedDomain' => 'mock_domain',
            'accessType' => 'mock_access_type'
        ]);
    }

    public function testAuthorizationUrl()
    {
        $url = $this->provider->getAuthorizationUrl();
        $uri = parse_url($url);
        parse_str($uri['query'], $query);

        $this->assertArrayHasKey('client_id', $query);
        $this->assertArrayHasKey('redirect_uri', $query);
        $this->assertArrayHasKey('state', $query);
        $this->assertArrayHasKey('scope', $query);
        $this->assertArrayHasKey('response_type', $query);
        $this->assertArrayHasKey('approval_prompt', $query);
        $this->assertArrayHasKey('hd', $query);
        $this->assertArrayHasKey('access_type', $query);

        $this->assertEquals('mock_access_type', $query['access_type']);
        $this->assertEquals('mock_domain', $query['hd']);

        $this->assertContains('https://www.googleapis.com/auth/youtube.readonly', $query['scope']);

        $this->assertAttributeNotEmpty('state', $this->provider);
    }

    public function testBaseAccessTokenUrl()
    {
        $url = $this->provider->getBaseAccessTokenUrl([]);
        $uri = parse_url($url);

        $this->assertEquals('/o/oauth2/token', $uri['path']);
    }

    public function testResourceOwnerDetailsUrl()
    {
        $token = $this->mockAccessToken();

        $url = $this->provider->getResourceOwnerDetailsUrl($token);
        $uri = parse_url($url);

        $this->assertEquals('/youtube/v3/channels', $uri['path']);
        $this->assertNotContains('mock_access_token', $url);
    }

    public function testResourceOwnerDetailsUrlCustomFields()
    {
        $provider = new YouTubeProvider([
            'clientId' => 'mock_client_id',
            'clientSecret' => 'mock_secret',
            'redirectUri' => 'none',
            'parts' => [
                'statistics',
            ],
        ]);

        $token = $this->mockAccessToken();

        $url = $provider->getResourceOwnerDetailsUrl($token);
        $uri = parse_url($url);
        parse_str($uri['query'], $query);

        $this->assertArrayHasKey('part', $query);
        $this->assertArrayHasKey('alt', $query);

        // Always JSON for consistency
        $this->assertEquals('json', $query['alt']);

        $parts = explode(',', $query['part']);

        // Default values
        $this->assertContains('id', $parts);
        $this->assertContains('snippet', $parts);

        // Configured values
        $this->assertContains('statistics', $parts);
    }

    public function testUserData()
    {
        // Mock
        $response = json_decode('{"kind": "youtube#channelListResponse","etag": "...","pageInfo": {"totalResults": 1,"resultsPerPage": 1},"items": [{"kind": "youtube#channel","etag": "...","id": "UC_mock_id","snippet": {"title": "Mock Title","description": "Mock description Lorem Ipsum","customUrl": "mock_url","publishedAt": "2010-01-01T12:00:00.000Z","thumbnails": {"default": {"url": "https://yt3.ggpht.com/mockpath/default/photo.jpg"},"medium": {"url": "https://yt3.ggpht.com/mockpath/medium/photo.jpg"},"high": {"url": "https://yt3.ggpht.com/mockpath/high/photo.jpg"}},"localized": {"title": "Mock Title","description": ""}}}]}', true);

        $token = $this->mockAccessToken();

        $provider = Phony::partialMock(YouTubeProvider::class);
        $provider->fetchResourceOwnerDetails->returns($response);
        $youtube = $provider->get();

        // Execute
        /** @var YouTubeChannel $channel */
        $channel = $youtube->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->fetchResourceOwnerDetails->called()
        );

        $this->assertInstanceOf('League\OAuth2\Client\Provider\ResourceOwnerInterface', $channel);

        $this->assertEquals('UC_mock_id', $channel->getId());
        $this->assertEquals('Mock Title', $channel->getTitle());
        $this->assertEquals('Mock Title', $channel->getName());
        $this->assertEquals('https://yt3.ggpht.com/mockpath/default/photo.jpg', $channel->getAvatarUrl());
        $this->assertEquals('https://yt3.ggpht.com/mockpath/default/photo.jpg', $channel->getAvatarUrl('default'));
        $this->assertEquals('https://yt3.ggpht.com/mockpath/medium/photo.jpg', $channel->getAvatarUrl('medium'));
        $this->assertEquals('https://yt3.ggpht.com/mockpath/high/photo.jpg', $channel->getAvatarUrl('high'));
        $this->assertEquals('https://yt3.ggpht.com/mockpath/default/photo.jpg', $channel->getAvatarUrl('invalid'));
        $this->assertEquals('Mock description Lorem Ipsum', $channel->getDescription());
        $this->assertEquals('mock_url', $channel->getCustomUrl());

        $channel = $channel->toArray();

        $this->assertEquals([
            "id" => "UC_mock_id",
            "title" => "Mock Title",
            "description" => "Mock description Lorem Ipsum",
            "customUrl" => "mock_url",
            "avatar" => [
                "default" => "https://yt3.ggpht.com/mockpath/default/photo.jpg",
                "medium" => "https://yt3.ggpht.com/mockpath/medium/photo.jpg",
                "high" => "https://yt3.ggpht.com/mockpath/high/photo.jpg",
            ],
        ], $channel);
    }

    public function testErrorResponse()
    {
        // Mock
        $error_json = '{"error": {"code": 400, "message": "I am an error"}}';

        $response = Phony::mock('GuzzleHttp\Psr7\Response');
        $response->getHeader->returns(['application/json']);
        $response->getBody->returns($error_json);

        $provider = Phony::partialMock(YouTubeProvider::class);
        $provider->getResponse->returns($response);

        $youtube = $provider->get();

        $token = $this->mockAccessToken();

        // Expect
        $this->expectException(IdentityProviderException::class);

        // Execute
        $channel = $youtube->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->getResponse->calledWith($this->instanceOf('GuzzleHttp\Psr7\Request')),
            $response->getHeader->called(),
            $response->getBody->called()
        );
    }

    public function testUnusualErrorResponse()
    {
        // Mock
        $error_json = '{"error": "I am an error"}';

        $response = Phony::mock('GuzzleHttp\Psr7\Response');
        $response->getHeader->returns(['application/json']);
        $response->getBody->returns($error_json);

        $provider = Phony::partialMock(YouTubeProvider::class);
        $provider->getResponse->returns($response);

        $youtube = $provider->get();

        $token = $this->mockAccessToken();

        // Expect
        $this->expectException(IdentityProviderException::class);

        // Execute
        $channel = $youtube->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->getResponse->calledWith($this->instanceOf('GuzzleHttp\Psr7\Request')),
            $response->getHeader->called(),
            $response->getBody->called()
        );
    }
    
    /**
     * @return AccessToken
     */
    private function mockAccessToken()
    {
        return new AccessToken([
            'access_token' => 'mock_access_token',
        ]);
    }
}