<?php

namespace CreatorsHub\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class YouTubeChannel implements ResourceOwnerInterface
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * Get channel id
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->response['items'][0]['id'] ?: null;
    }

    /**
     * Get channel avatar url
     *
     * @param string $size
     *              default – The default thumbnail image.
     *                        The default thumbnail for a channel is 88px wide and 88px tall.
     *              medium – A higher resolution version of the thumbnail image.
     *                       For a channel, this image is 240px wide and 240px tall.
     *              high – A high resolution version of the thumbnail image.
     *                     For a channel, this image is 800px wide and 800px tall
     *
     * @return null|string
     */
    public function getAvatarUrl($size = 'default')
    {
        $sizes = ['default', 'medium', 'high'];

        if (!in_array($size, $sizes)) {
            $size = 'default';
        }

        return $this->response['items'][0]['snippet']['thumbnails'][$size]['url'] ?: null;
    }

    /**
     * Get channel name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getTitle();
    }

    /**
     * Get channel name
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->response['items'][0]['snippet']['title']  ?: null;
    }

    /**
     * Get channel description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->response['items'][0]['snippet']['description'] ?: null;
    }

    /**
     * Get channel custom url
     */
    public function getCustomUrl()
    {
        return $this->response['items'][0]['snippet']['customUrl'] ?: null;
    }

    /**
     * Get user data as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'customUrl' => $this->getCustomUrl(),
            'avatar' => [
                'default' => $this->getAvatarUrl(),
                'medium' => $this->getAvatarUrl('medium'),
                'high' => $this->getAvatarUrl('high'),
            ],
        ];
    }
}
